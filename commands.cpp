#include "commands.h"
#include <iostream>
#include <iomanip>
#include <list>
#include <unistd.h>
#include <cstdlib>
#include <ctime>
#include "globals.h"
#include <string>

void commands::mkdir(File *currentDir, string dirName)
{
    // Verifica se o nome da pasta não existe
    if(currentDir->getDirectoryTable()->findFile(dirName) != NULL) {
        std::cout << dirName << ": esse diretório já existe" << std::endl;
        return;
    }
    // Faz verificação se o diretório já não contem 255 arquivos.
    if(currentDir->getFile_type().root && currentDir->getDirectoryTable()->getChildren()->size() == 255) {
        std::cout << "diretório root cheio (255 arquivos)" << std::endl;
    } else {
        File *newDir = new File;
        newDir->setFile_name(dirName);
        FileType fileType;
        fileType.directory=true;
        time_t t = time(0); // get time now
        date now = localtime(&t);

        newDir->setFile_type(fileType);
        newDir->setCreation_date(asctime(now));
        newDir->setDirectoryTable(new DirectoryTable);
        newDir->setParent(currentDir);

        currentDir->getDirectoryTable()->getChildren()->push_back(newDir);
    }
}

void commands::dir(File *currentDir)
{
    File *temp;
    for(unsigned int i = 0; i < currentDir->getDirectoryTable()->getChildren()->size(); i++) {
        temp = currentDir->getDirectoryTable()->getChildren()->at(i);
        if(temp->getFile_type().directory) {
            std::cout << "[" << temp->getFile_name() << "]" << std::endl;
        } else {
            std::cout << temp->getFile_name() << std::endl;
        }
    }
}

void commands::dirFile(File *file)
{
    std::cout << "Nome: " << file->getFile_name() << std::endl;
    std::cout << "Tamanho Real: " << std::setprecision(2) << file->getFile_size() / 1000.0 << " KB (" << file->getFile_size() << " bytes)" << std::endl;
    unsigned int cluster_count = file->getFile_size() / CLUSTER_SIZE;
    if(file->getFile_size() % CLUSTER_SIZE) cluster_count++;
    std::cout << "Tamanho em memória: " << cluster_count * CLUSTER_SIZE / 1000.0 << " KB (" << cluster_count << " cluster(s))" << std::endl;
    std::cout << "Data de criação: " << file->getCreation_date();
}

void commands::cd(File **currentDir, string nextDir)
{
    //string::compare returns 0 if equal
    if(nextDir.compare("..") == 0) {
        if ((*currentDir)->getParent() == NULL) {
            std::cout << "já está na pasta root!" << std::endl;
        } else {
            *currentDir = (*currentDir)->getParent();
        }
    } else {
        File *temp;
        for(unsigned int i = 0; i < (*currentDir)->getDirectoryTable()->getChildren()->size(); i++) {
            temp = (*currentDir)->getDirectoryTable()->getChildren()->at(i);
            if(temp->getFile_type().directory && !temp->getFile_name().compare(nextDir)) {
                *currentDir = temp;
                return;
            }
        }
        std::cout << nextDir <<  ": diretório não econtrado" << std::endl;
    }
}

std::vector<string>* commands::parseCommand(string command)
{
    std::vector<string> *tokens = new std::vector<string>;
    string temp = "";
    for(unsigned int i = 0; i < command.length(); i++) {
        if(command.at(i) == ' ') {
            tokens->push_back(temp);
            temp = "";
        } else {
            temp += command.at(i);
        }
    }
    if(temp != "") tokens->push_back(temp);
    return tokens;
}

void commands::copyFile(File *currentDir, string fileName, string newFileName)
{
    if(currentDir->getDirectoryTable()->findFile(newFileName) == NULL) {
        ifstream file;
        file.open(fileName.c_str(), ios::ate | ios::binary);
        if(file.is_open()) {
            File *newFile = new File;
            newFile->setFile_size(file.tellg());
            newFile->setFile_name(newFileName);
            time_t t = time(0); // get time now
            date now = localtime(&t);
            newFile->setCreation_date(asctime(now));
            // Aloca o espaço necessário na FAT para comportar o arquivo
            if(fat->allocateFile(newFile) ) {
                currentDir->getDirectoryTable()->getChildren()->push_back(newFile);
                disc.open("disc.dat", ios::binary | ios::out | ios::in);

                char *block;
                block = new char[CLUSTER_SIZE];
                file.seekg(0, ios::beg);
                int i;
                // Grava o arquivo nas partes alocadas
                cout << "Gravando arquivo..." << endl;
                cout << "-------------------"<< endl;
                for(i = newFile->getStarting_cluster(); fat->table[i].nextCluster < MAX_CLUSTER; i = fat->table[i].nextCluster) {
                    file.read(block, CLUSTER_SIZE);
                    disc.seekp(i*CLUSTER_SIZE, ios::beg);
                    disc.write(block, CLUSTER_SIZE);
                    cout << block << endl;
                }
                file.read(block, CLUSTER_SIZE);
                disc.seekp(i*CLUSTER_SIZE, ios::beg);
                disc.write(block, CLUSTER_SIZE);
                file.close();
                disc.close();
                cout << block << endl;
                cout << "-------------------"<< endl;
                cout << "Fim da gravação" << endl;
            } else {
                std::cout << "erro: sem espaço em disco" << std::endl;
            }
        } else {
            copyFromFat(currentDir, fileName, newFileName);
        }
    } else {
        std::cout << newFileName << ": nome de arquivo em uso" << std::endl;
    }
}

void commands::type(File *file)
{
    char *block;
    block = new char[CLUSTER_SIZE];
    disc.open("disc.dat", ios::binary | ios::out | ios::in);
    int i;
    // Vai lendo os blocos correspondentes ao arquivo da FAT.
    for(i = file->getStarting_cluster(); fat->table[i].nextCluster < MAX_CLUSTER; i = fat->table[i].nextCluster) {
        // Continha que o prof. ensinou
        // para encontrar o bloco correspondente, i [numero do bloco]
        // vezes o tamanho do bloco.
        disc.seekg(i*CLUSTER_SIZE, ios::beg);
        // lê para uma variavel de caracteres
        disc.read(block,CLUSTER_SIZE);
        std::cout << block << std::endl;
    }
    disc.seekg(i*CLUSTER_SIZE, ios::beg);
    disc.read(block,CLUSTER_SIZE);
    disc.close();
    std::cout << block << std::endl;
}


File *commands::startRoot()
{
    /*  Inicializa o diretório de root  */
    File *rootDir = new File;
    FileType rootType;
    rootType.directory = true;
    rootType.root = true;
    rootDir->setFile_type(rootType);
    rootDir->setFile_name("/");
    rootDir->setDirectoryTable(new DirectoryTable);
    rootDir->setParent(NULL);
    disc.open("disc.dat", ios::in | ios::trunc);
    disc.close();

    return rootDir;
}


void commands::cp(File *currentDir, string fileName)
{
    string newFileName = fileName.substr(fileName.find_last_of('/')+1, 255);
    if(newFileName.compare(fileName) == 0) {
        std::cout << "faltando nome do arquivo" << std::endl;
    } else {
        copyFile(currentDir, fileName, newFileName);
    }
}

void commands::cp(File *currenDir, string fileName, string newFileName)
{
    copyFile(currenDir, fileName, newFileName);
}

void commands::copyFromFat(File *currentDir, string fileName, string newFileName)
{
    File *file = currentDir->getDirectoryTable()->findFile(fileName);
    
    // Tenta "abrir" o arquivo da partição
    if(file != NULL) {
        File *newFile = new File;
        newFile->setFile_name(newFileName);
        newFile->setFile_size(file->getFile_size());
        time_t t = time(0); // get time now
        date now = localtime(&t);
        newFile->setCreation_date(asctime(now));

        // Chama função de alocar espaço na partição
        if(fat->allocateFile(newFile) ) {
            currentDir->getDirectoryTable()->getChildren()->push_back(newFile);
            disc.open("disc.dat", ios::binary | ios::out | ios::in);

            char *block;
            block = new char[CLUSTER_SIZE];
            int i, j;
            // Faz a copia dos dados, para cada "parte" do arquivo, procura seu
            // bloco correspondente alocado pela função anterior
            for(i = file->getStarting_cluster(), j = newFile->getStarting_cluster(); fat->table[i].nextCluster < MAX_CLUSTER; i = fat->table[i].nextCluster, j = fat->table[j].nextCluster) {
                disc.seekg(i*CLUSTER_SIZE, ios::beg);
                disc.read(block, CLUSTER_SIZE);
                disc.seekp(j*CLUSTER_SIZE, ios::beg);
                disc.write(block, CLUSTER_SIZE);
            }
            // Repete o processo para o ultimo bloco
            disc.seekg(i*CLUSTER_SIZE, ios::beg);
            disc.read(block, CLUSTER_SIZE);
            disc.seekp(j*CLUSTER_SIZE, ios::beg);
            disc.write(block, CLUSTER_SIZE);
            disc.close();
        } else {
            std::cout << "erro: sem espaço em disco" << std::endl;
        }
    } else {
        std::cout << fileName << ": arquivo não encontrado" << std::endl;
    }
}


void commands::help()
{
    std::cout << "help                   | mostra a ajuda" << std::endl << std::endl;
    std::cout << "format                 | formata a partição, tome cuidado" << std::endl << std::endl;
    std::cout << "mkdir [nome da pasta]  | cria um novo diretório" << std::endl << std::endl;
    std::cout << "cd [nome da pasta]     | muda para o diretório escolhido. 'cd ..' volta para o diretório anterior\n" << std::endl << std::endl;
    std::cout << "dir [file_name]        | mostra todos os arquivos que pertencem a este diretório. se um nome de arquivo for passado como parâmetro,\n"
              << "                       | mostra todas as informações referentes ao arquivo" << std::endl << std::endl;
    std::cout << "cp [oridemr] [destino] | copia o arquivo de dentro pra fora da partição. Se o arquivo for\n"
              << "                       | de fora da particao, o caminho inteiro dever se passdo e o parametro nome é opcional" << std::endl << std::endl;
    std::cout << "type [nome do arquivo  | mostra o conteudo do arquivo." << std::endl;
}
