#include "fat.h"
#include <iostream>

Fat::Fat()
{
}

bool Fat::allocateFile(File *file)
{
    unsigned int cluster_count = file->getFile_size() / CLUSTER_SIZE;
    unsigned int aux[MAX_CLUSTER];

    if(file->getFile_size() % CLUSTER_SIZE) {
        cluster_count++;
    }
    // Tenta encontrar clusters disponiveis
    aux[0] = seekFreeCluster(0);
    if(aux[0] == MAX_CLUSTER) {
        // no clusters avaiable
        return false;
    }
    // Verifica se o arquivo cabe no sistema, pra isso vai alocando os espaços 
    // necessários, caso o arquivo não couber no sistema, uma mensgem de erro
    // é retornada.
    for(unsigned int i = 1; i < cluster_count; i++) {
        aux[i] = seekFreeCluster(aux[i-1]+1);
        if(aux[i] == MAX_CLUSTER) {
            // no clusters avaiable
            return false;
        }
    }
    // Seta na struc do arquivo qual é o cluster inicial
    file->setStarting_cluster(aux[0]);
    // Seta na tabela de partições que o cluster não está mais disponivel
    table[aux[0]].free = false;
    // Aloca os clusters necessários para o arquivo
    for(unsigned int i = 1; i < cluster_count; i++) {
        table[aux[i-1]].nextCluster = aux[i];
        table[aux[i]].free = false;
    }
    table[aux[cluster_count-1]].nextCluster = MAX_CLUSTER;
    return true;
}

unsigned int Fat::seekFreeCluster(unsigned int initial)
{
    unsigned int i;
    for(i = initial; i < MAX_CLUSTER && (!table[i].free); i++);

    return i;
}
