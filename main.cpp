#include <iostream>
#include <fstream>
#include <unistd.h>
#include <vector>
#include "fat.h"
#include "file.h"
#include "commands.h"
#include "globals.h"

using namespace std;

int main() {
    /*  Iniciando diretório root  */
    File *rootDir = commands::startRoot();
    fat = new Fat;
    File **currentDir = &rootDir;
    cout << "Digite help para Ajuda" << endl;

    vector<string> *command_args;
    string input;

    while (true) {
        input.clear();
        // Toda vez que o loop volta, escreve o caminho atual 
        while (input.empty()) {
            cout << "lucas@sistemasoperacionais~ " << (*currentDir)->getFile_name() << " $ ";
            getline(std::cin, input);
        }
        
        command_args = commands::parseCommand(input);
        input = command_args->front();
        
        // Valida sintaxe/parametros da função "mkdir" e a executa a seguir.
        if (input.compare("mkdir") == 0) {
            if (command_args->size() > 1) commands::mkdir(*currentDir, command_args->at(1));
            else cout << "por favor, insira o nome da pasta a ser criada" << endl;
        }
        // Valida parâmetros da função "dir" e a executa a seguir
        else if (input.compare("dir") == 0 || input.compare("ls") == 0) {
            if (command_args->size() > 1) {
                File *file;
                file = (*currentDir)->getDirectoryTable()->findFile(command_args->at(1));
                if (file == NULL) {
                    cout << command_args->at(1) << ": Arquivo não encontrado" << endl;
                } else {
                    commands::dirFile(file);
                }
            } else {
                commands::dir(*currentDir);
            }
        } 
        // Valida parametros da função cd e a executa a seguir
        else if (input.compare("cd") == 0) {
            if (command_args->size() > 1) commands::cd(currentDir, command_args->at(1));
            else cout << "faltando o nome do diretório" << endl;
        } else if (input.compare("cp") == 0) {
            if (command_args->size() > 2) commands::cp(*currentDir, command_args->at(1), command_args->at(2));
            else if (command_args->size() == 2) commands::cp(*currentDir, command_args->at(1));
            else cout << "faltando o caminho do arquivo" << endl;
        }
        
        // Valida parametros da função type e a executa a seguir
        else if (input.compare("type") == 0) {
            if (command_args->size() > 1) {
                File *file;
                file = (*currentDir)->getDirectoryTable()->findFile(command_args->at(1));
                if (file == NULL) {
                    cout << command_args->at(1) << ": Arquivo não encontrado" << endl;
                } else {
                    if (file->getFile_type().directory) {
                        cout << file->getFile_name() << " é um diretório" << endl;
                    } else {
                        commands::type(file);
                    }
                }
            } else {
                cout << "faltando nome do arquivo" << endl;
            }
        } 
        // Valida parametros da função format e a executa a seguir
        else if (input.compare("format") == 0) {
            delete fat;
            delete rootDir;
            rootDir = commands::startRoot();
            fat = new Fat;
            currentDir = &rootDir;
            cout << "Sistema formatado com sucesso" << endl;
        } else if (input.compare("help") == 0) {
            commands::help();
        }
        else if (input.compare("exit") == 0) {
            return 0;
        }
        else {
            cout << command_args->front() << ": comando não encontrado!" << endl;
        }
    }

    return 0;
}
